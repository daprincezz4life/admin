# Admin

This repo is for internal team requests that can only be taken care of
by people with privileges on the Gitlab group or access to prod
machines.

For new requests, use the [issue tracker](https://gitlab.com/fdroid/admin/issues).

### General rules

* Anyone who wishes to help or already has access to a repo should get
  Reporter permissions on the whole group
* Anyone who has contributed code to a repo and can be trusted can be
  granted Developer permissions on that repo
* The number of owners of the whole group should be kept at a minimum,
  but above 1-2 people to avoid a single point of failure
